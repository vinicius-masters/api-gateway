#!/bin/bash

DEFAULT_KAFKA="192.168.0.100"
OSV_IP="192.168.122.82"
PORT="33000"

ARG1=${1:-KAFKA_BROKER=$DEFAULT_KAFKA}
ARG2=${2:-API_GATEWAY=$OSV_IP}
ARG3=${3:-PORT=$PORT}

sudo make clean && sudo capstan rmi api-gateway && sudo capstan run -e '--env='$ARG1' --env='$ARG2' /python.so /api-gateway/api_gateway.py' -n bridge -f "$PORT:$PORT" --mac 82:DA:1E:11:3F:55

