import json
import falcon

from os import environ
from wsgiref import simple_server
from pyjung import JungTasker


class AuthMiddleware(JungTasker):

    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="api-gateway"):
        super().__init__(in_topic, out_topic, ip, port, group)

    def process_request(self, req, resp):
        # POST to /users cannot be authenticated
        if req.path != "/users" and req.method != "POST":
            password = req.get_header("password")
            username = req.path.split("/users/")[1].split("/")[0]

            if username is not None and password is not None:
                user_auth = {
                    "username": username,
                    "password": password,
                    "device_id": None
                }

                # check if it is a device related request
                parse_devices_url = req.url.split("devices/")
                if len(parse_devices_url) > 1:
                    device_id = parse_devices_url[1].split("/")
                    if len(device_id) > 0:
                        device_id = device_id[0]
                        user_auth["device_id"] = device_id

                user_auth = self.create_task("AUTH", user_auth)
                self.publish(user_auth)
                response = self.get_task_result(user_auth["id"])
                if response is not None and response["auth"] == False:
                    raise falcon.HTTPUnauthorized('Unauthorized')
            else:
                raise falcon.HTTPUnauthorized('Authentication required')


class UserRegistry(JungTasker):
    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="api-gateway"):
        super().__init__(in_topic, out_topic, ip, port, group)

    def on_get(self, req, resp, username=None):
        """Get Kafka messages"""
        if username is not None:
            try:
                task = self.create_task("GET", {"username": username})
                self.publish(task)
                resp.media = self.get_task_result(task["id"])
            except Exception as e:
                raise falcon.HTTPError(falcon.HTTP_400, "Error", e.args)

    def on_post(self, req, resp):
        """"Add Kafka messages by topic"""
        try:
            task = self.create_task("CREATE", req.media)
            self.publish(task)
            resp.media = self.get_task_result(task["id"])
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400, "Error", e.args)


class DeviceRegistry(JungTasker):
    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="api-gateway"):
        super().__init__(in_topic, out_topic, ip, port, group)

    def on_get(self, req, resp, username, device_id=None):
        """Get Kafka messages"""

        if device_id is not None:
            try:
                self.reset_partitions()
                task = self.create_task("GET", {"device_id": device_id})
                self.publish(task)
                resp.media = self.get_task_result(task["id"])
            except Exception as e:
                raise falcon.HTTPError(falcon.HTTP_400, "Error", e.args)

    def on_post(self, req, resp, username):
        """"Add Kafka messages by topic"""
        try:
            task = self.create_task("CREATE", req.media)
            self.publish(task)
            resp.media = self.get_task_result(task["id"])
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400, "Error", e.args)


class DeviceMonitor(JungTasker):

    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="api-gateway"):
        super().__init__(in_topic, out_topic, ip, port, group)

    def on_get(self, req, resp, username, device_id):
        """Get device readings"""

        # FIXME: without this partition reset, it never gets the responses. Why?!
        self.reset_partitions()

        try:
            task = self.create_task("READ", {"device_id": device_id, "page_limit":10})
            self.publish(task)
            resp.media = self.get_task_result(task["id"])
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400, "error", e.args)


class DeviceCommander(JungTasker):

    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="api-gateway"):
        super().__init__(in_topic, out_topic, ip, port, group)

    def on_post(self, req, resp, username, device_id):
        """Send a command to a device"""

        try:
            command = req.media
            command["device_id"] = device_id
            task = self.create_task("COMMAND", command)
            self.publish(task)
            resp.media = self.get_task_result(task["id"])
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400, "error", e.args)


class RuleEngine(JungTasker):
    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="api-gateway"):
        super().__init__(in_topic, out_topic, ip, port, group)

    def on_get(self, req, resp, username, device_id):
        """Get rules"""
        try:
            self.reset_partitions()
            task = self.create_task("GET", {"device_id": device_id})
            self.publish(task)
            resp.media = self.get_task_result(task["id"])
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400, "Error", e.args)

    def on_post(self, req, resp, username, device_id):
        """"Create rules"""
        try:
            rule = req.media
            rule["device_id"] = device_id
            task = self.create_task("CREATE", rule)
            self.publish(task)
            resp.media = self.get_task_result(task["id"])
        except Exception as e:
            raise falcon.HTTPError(falcon.HTTP_400, "Error", e.args)



kafka_broker = environ["KAFKA_BROKER"]

auth_manager = AuthMiddleware(ip=kafka_broker,
                                in_topic="auth_results",
                                out_topic="auth_tasks")
user_registry = UserRegistry(ip=kafka_broker,
                                in_topic="user_results",
                                out_topic="user_tasks")
device_registry = DeviceRegistry(ip=kafka_broker,
                                    in_topic="device_results",
                                    out_topic="device_tasks")
device_monitor = DeviceMonitor(ip=kafka_broker,
                                in_topic="reading_results",
                                out_topic="reading_tasks")
device_commander = DeviceCommander(ip=kafka_broker,
                                    in_topic="command_results",
                                    out_topic="command_tasks")
rule_engine = RuleEngine(ip=kafka_broker,
                            in_topic="rule_results",
                            out_topic="rule_tasks")

api = falcon.API(middleware=[auth_manager])

api.add_route('/users', user_registry)
api.add_route('/users/{username}', user_registry)
api.add_route('/users/{username}/devices', device_registry)
api.add_route('/users/{username}/devices/{device_id}', device_registry)
api.add_route('/users/{username}/devices/{device_id}/readings',
                device_monitor)
api.add_route('/users/{username}/devices/{device_id}/commands',
                device_commander)
api.add_route('/users/{username}/devices/{device_id}/rules', rule_engine)

def main():
    port = 33000
    ip = environ["API_GATEWAY"]

    httpd = simple_server.make_server(ip, port, api)
    print("starting server {}:{}...".format(ip, port))
    httpd.serve_forever()


if __name__ == '__main__':
    main()
