FROM jung-base

ADD src /src

RUN pip install falcon

ENV KAFKA_BROKER 192.168.1.6
ENV API_GATEWAY localhost

CMD python src/api_gateway/api_gateway.py
